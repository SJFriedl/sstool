// sstool.cpp
//
// written by : Stephen J Friedl
//              Software Consultant
//              Southern California, USA
//              steve@unixwiz.net
//
//  This program has the very narrow task of manipulating the allocation of session
//  desktop heap sizes, which is managed by the registry. The registry value itself
//  is quite long and easy (and dangerous) to get wrong, so this attempts to be a
//  front end that nominally does the right thing with complete error checking.
//
//  THis very long string (from my Windows 10 machine) looks like:
//
//      %SystemRoot%\system32\csrss.exe ObjectDirectory=\Windows
//          SharedSection=1024,20480,768 Windows=On SubSystemType=Windows
//          ServerDll=basesrv,1 ServerDll=winsrv:UserServerDllInitialization,3
//          ServerDll=sxssrv,4 ProfileControl=Off MaxRequestThreads=16
//
//  but all in one line. What we care about is the SharedSection token, where
//  we normally need to modify the last number by increasing 768 to a larger
//  value. I believe Windows won't boot if this registry entry is dorked.

#include <windows.h>
#include <stdlib.h>
#include <assert.h>
#include <memory.h>
#include <list>

// Key found in HKLM
static const wchar_t keyname[] = L"SYSTEM\\CurrentControlSet\\Control\\Session Manager\\SubSystems";
static const wchar_t valname[] = L"Windows";

static size_t reassemble(wchar_t *outbuf, int bufsize, std::list<wchar_t *> tokens);

#define COUNTOF(x)  (sizeof(x) / sizeof( *(x)) )

int wmain(int argc, wchar_t **argv)
{
	(void)argc; // shut up compiler warning

    HKEY hKeySubsys;
    bool writable = false;

    LSTATUS status;

    // Open the home registry key. First try to open with write permissiohs, but if
    // not, fall back to read and make a note that we're in readonly mode.

#define OPENKEY_RW  (KEY_READ | KEY_SET_VALUE)
#define OPENKEY_RO  (KEY_READ)

    REGSAM openMode = OPENKEY_RW;

    writable = true;

    while ((status = RegOpenKeyExW(HKEY_LOCAL_MACHINE,
    	keyname,        // SYSTEM\CurrentControlSet\...\SubSystems
    	0,              // options
    	openMode,
    	&hKeySubsys)) != ERROR_SUCCESS)
    {
    	// Ok, it failed - why?  If it's access denied, we might try one more
    	// round after lowering requested access rights. But if it's some *other*
    	// error, then report it and exit with a reasonable message.

    	if (status != ERROR_ACCESS_DENIED)
    	{
    		fwprintf(stderr, L"ERROR: %s: cannot open reg key %s; err#=%ld\n",
    			argv[0],
    			keyname,
    			status);
    		exit(EXIT_FAILURE);
    	}
    	else if (openMode == OPENKEY_RW)
    	{
    		fwprintf(stderr, L"Note: cannot open regkey R/W, trying readonly\n");

    		openMode = OPENKEY_RO;
    	}
    	else
    	{
    		assert(openMode == OPENKEY_RO);

    		fwprintf(stderr, L"ERROR: %s: unable to open regkey %s; Error=ERROR_ACCESS_DENIED\n",
    			argv[0], keyname);
    		exit(EXIT_FAILURE);
    	}
    }

    fwprintf(stderr, L"Regkey %s now open\n", keyname);

    // Now read the value of that long string; by allocating a local
    // buffer that is *so much* larger than needed, we don't have
    // to manage memory allocations - it's impossible that one of
    // these will be that larger.

    wchar_t ssbuf[10240];
    DWORD ssbufsize = sizeof ssbuf;
    DWORD keytype;

    if ((status = RegQueryValueExW(hKeySubsys,
    	valname,
    	0,          // lpReserved
    	&keytype,   // lpType
    	(LPBYTE)ssbuf, &ssbufsize)) != ERROR_SUCCESS)
    {
    	// 234 = ERROR_MORE_DATA; ssbuf not big enough?
    	fwprintf(stderr, L"ERROR: %s: cannot ReqQueryValue \"%s\": error=%ld\n", argv[0], valname, status);
    	exit(EXIT_FAILURE);
    }

    // split 
    wchar_t tokbuf[sizeof ssbuf];

    wcscpy_s(tokbuf, COUNTOF(tokbuf), ssbuf);

    wchar_t *tokp, *bufp = tokbuf, *context = nullptr;

    std::list<wchar_t *> tokens;

    wprintf(L"Regkey %s\\%s contains:\n", keyname, valname);

    while ((tokp = wcstok_s(bufp, L" ", &context)) != nullptr)
    {
		wprintf(L"--> %s\n", tokp);
    	tokens.push_back(tokp);
    	bufp = nullptr;
    }

    // This is where we reassemble the tokens back into a string, we need to be
    // super careful we can reassemble it *exactly* c
    wchar_t reassemblebuf[COUNTOF(ssbuf)];

	const size_t resize = reassemble(reassemblebuf, COUNTOF(reassemblebuf), tokens);

	if (wcscmp(ssbuf, reassemblebuf) != 0)
	{
		fwprintf(stderr, L"ERROR: token reassembly error!\n");
		fwprintf(stderr, L"Orig: %d/%s\n", wcslen(ssbuf), ssbuf);
        fwprintf(stderr, L"New:  %d/%s\n", wcslen(reassemblebuf), reassemblebuf);
	}
	fwprintf(stderr, L"Token reassembly ok\n");

    // Ok, so now we are *sure* that we can exactly rebuild the string as-is,
    // so we're able to do our thing.

    exit(EXIT_SUCCESS);
}

// token iterator


static size_t reassemble(wchar_t *outbuf, int bufsize, std::list<wchar_t *> tokens)
{
	wchar_t *bufnext = outbuf,
		*bufmax = outbuf + bufsize;

    for (auto s : tokens)
    {
    	const size_t toksize = wcslen(s);

    	if (bufnext > outbuf)
    		*bufnext++ = ' ';

    	const size_t nleft = bufmax - bufnext; // space available

		wcscpy_s(bufnext, nleft, s);

		bufnext += toksize;

		*bufnext = 0;
    }

	return bufnext - outbuf;
}